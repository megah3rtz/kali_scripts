#!/bin/bash

apt-get update -y
apt-get upgrade -y

apt-get install terminator -y

sed -i 's/XKBLAYOUT=".*"/XKBLAYOUT="gb"/' /etc/default/keyboard

echo "Europe/London" > /etc/timezone
ln -sf "/usr/share/zoneinfo/Europe/London" /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
